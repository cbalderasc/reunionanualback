<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Slim\Http\UploadedFile;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$configuration = [
    'settings' => [
        'displayErrorDetails' => true,
    ],
];

$c = new \Slim\Container($configuration);
$app = new \Slim\App($c);
$session = "Hey";

$app->add(new \Slim\Middleware\Session([
    'name' => 'session_fnsl',
    'autorefresh' => true,
    'lifetime' => '1 hour'
]));

$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
        ->withHeader('Access-Control-Allow-Origin', '*')
        ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
        ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
});

/**
 * TODO:
 * - Actualizar la sesión para que guarde datos específicos y no todo
 * - Ver que las sesiones no cambien desde la request
 */

//Validar que la cadena no venga vacía o nula
function IsNullOrEmptyString($str) {
    return (!isset($str) || trim($str) === '');
}

//Validar que tenga los parámetros necesarios
function validaParametros($parametros, $origen) {
    $isValid = true;
    $arr = array();
    foreach ($parametros as $key => $value) {
        if ($origen) {
            if ($key == "t") {
                continue;
            } 
            if ($key == "in") {
                continue;
            }
            if ($key == "f") {
                continue;
            }
        }
        if (IsNullOrEmptyString($value)) {
            $isValid = false;
            break;
        }
    }
    return $isValid;
}

//Obtener disponibilidad de la plataforma
function checkAvailability($request) {
    $sql = "SELECT activo FROM actividad LIMIT 1";
    try{
        $db = new db();
        $db = $db->conectDB();
        $resultado = $db->prepare($sql);
        $resultado->execute();

        if($resultado->rowCount() > 0){
            $disp = $resultado->fetchAll(PDO::FETCH_ASSOC);
            $available = boolval($disp[0]['activo']);
        }else{
            $mensaje = "Hubo un error en la base de datos";
            $arrRes = array("ok" => false, "data" => $mensaje);
            echo json_encode($arrRes);
            return;
        }

        if (!$available) {
            $url = $request->getUri();
            $baseUrl = $url->getBaseUrl();
            $hostWithEv = explode('//', $baseUrl);
    
            $hostnamePrev = explode('/',$hostWithEv[1]);
            $protocol = $hostWithEv[0].'//';
            $hostname = $hostnamePrev[0];
            $complement = "";
            if (strpos($hostname, 'cozcyt') !== false) {
                $complement = "/labsol";
            }
            $urlComp = $protocol . $hostname . $complement . '/fnsl/plataforma/404.html';
            return $urlComp;
        } else {
            return false;
        }

        $resultado = null;
        $db = null;
    }catch(Exception $e){
        $arr = array("ok" => false, "data" => $e->getMessage());
        echo json_encode($arr);
        return false;
    }
    
}

//Validar que la sesión no ha cambiado desde la petición con los parámetros entrantes
function checkForValidSession($id_asistente) {

}

//Obtener estados
$app->get('/api/estados', function(Request $request, Response  $response) use($app){
    $validation = checkAvailability($request);
    
    if ($validation !== false) {
        return $response->withRedirect($validation, 302);
    }
    $sql = "SELECT * FROM estado";

    try{
        $db = new db();
        $db = $db->conectDB();
        $resultado = $db->prepare($sql);
        $resultado->execute();

        if($resultado->rowCount() > 0){
            $estados = $resultado->fetchAll(PDO::FETCH_ASSOC);
            $arrRes = array("ok" => true, "data" => $estados);
            echo json_encode($arrRes);
        }else{
            $mensaje = "No existen estados en la BD";
            $arrRes = array("ok" => false, "data" => $mensaje);
            echo json_encode($arrRes);
        }

        $resultado = null;
        $db = null;
    }catch(Exception $e){
        $arr = array("ok" => false, "data" => $e->getMessage());
        echo json_encode($arr);
    }
});

//Obtener municipios
$app->get('/api/municipios', function(Request $request, Response  $response) use($app){
    $queryParam = $request->getQueryParams();
    $idEstado = $queryParam['id'];

    $validation = checkAvailability($request);
    
    if ($validation !== false) {
        return $response->withRedirect($validation, 302);
    }

    $sql = "SELECT * FROM municipio WHERE id_estado = :id";

    try{
        $db = new db();
        $db = $db->conectDB();
        $resultado = $db->prepare($sql);
        $resultado->bindParam(':id', $idEstado);
        $resultado->execute();

        if($resultado->rowCount() > 0){
            $municipios = $resultado->fetchAll(PDO::FETCH_ASSOC);
            $arrRes = array("ok" => true, "data" => $municipios);
            echo json_encode($arrRes);
        }else{
            $mensaje = "No existen estados en la BD";
            $arrRes = array("ok" => false, "data" => $mensaje);
            echo json_encode($arrRes);
        }

        $resultado = null;
        $db = null;
    }catch(Exception $e){
        $arr = array("ok" => false, "data" => $e->getMessage());
        echo json_encode($arr);
    }
});

//Registrar asistente
$app->post('/api/rasistente', function(Request $request, Response $response) use($app){
    $queryParam = $request->getQueryParams();
    $nombre = $queryParam['n'];
    $institucion = $queryParam['i'];
    $perfil = $queryParam['p'];
    $facebook = $queryParam['f'];
    $twitter = $queryParam['t'];
    $instagram = $queryParam['in'];
    $idEstado = $queryParam['ide'];
    $idMunicipio = $queryParam['idm'];
    $contra = $queryParam['cn'];
    $correo = $queryParam['cr'];

    $validation = checkAvailability($request);
    
    if ($validation !== false) {
        return $response->withRedirect($validation, 302);
    }

    if (!validaParametros($queryParam, true)) {
        $mensaje = "Valores no validos";
        $arrRes = array("ok" => false, "data" => $mensaje);
        echo json_encode($arrRes);
        return;
    }
    
    $b64Contra = urldecode($contra);
    $pass_decode = base64_decode($b64Contra);
    $pass_for_db = sha1($pass_decode);
    
    $sql = "INSERT INTO asistente (nombre, inst_procedencia, perfil_academico, facebook, twitter, instagram, id_estado, id_municipio, contra, correo) VALUES(:nombre, :inst, :perfil, :face, :twitter, :insta, :ide, :idm, :contra, :correo)";
    $sqlConfirm = "SELECT correo FROM asistente WHERE correo = :correo";

    try{
        $dbCorreo = new db();
        $dbCorreo = $dbCorreo->conectDB();
        $resultadoConfirm = $dbCorreo->prepare($sqlConfirm);
        $resultadoConfirm->bindParam(':correo', $correo);
        $resultadoConfirm->execute();

        if($resultadoConfirm->rowCount() > 0){
            $mensaje = "Este correo ya está registrado";
            $arrRes = array("ok" => false, "data" => $mensaje);
            echo json_encode($arrRes);
            return;
        }

        $db = new db();
        $db = $db->conectDB();
        $resultado = $db->prepare($sql);
        $resultado->bindParam(':nombre', $nombre);
        $resultado->bindParam(':inst', $institucion);
        $resultado->bindParam(':perfil', $perfil);
        $resultado->bindParam(':face', $facebook);
        $resultado->bindParam(':twitter', $twitter);
        $resultado->bindParam(':insta', $instagram);
        $resultado->bindParam(':ide', $idEstado);
        $resultado->bindParam(':idm', $idMunicipio);
        $resultado->bindParam(':contra', $pass_for_db);
        $resultado->bindParam(':correo', $correo);
        $resultado->execute();

        $res = array("ok" => true, "data" => "El usuario ha sido agregado con éxito");
        echo json_encode($res);

        $resultado = null;
        $db = null;
    }catch(Exception $e){
        $arr = array("ok" => false, "data" => $e->getMessage());
        echo json_encode($arr);
    }
});

//Iniciar sesión
$app->post('/api/login', function (Request $request, Response  $response) use ($app) {
    $parametros = $request->getQueryParams();
    $correo = $parametros['cr'];
    $contra = $parametros['cn'];

    $validation = checkAvailability($request);
    
    if ($validation !== false) {
        return $response->withRedirect($validation, 302);
    }

    if (!validaParametros($parametros, false)) {
        $mensaje = "Usuario y/o contraseña incorrectos";
        $arrRes = array("ok" => false, "data" => $mensaje);
        echo json_encode($arrRes);
        return;
    }

    $sql = "SELECT nombre, id_asistente, id_rango FROM asistente WHERE correo = :correo AND contra = :contra";

    $b64Contra = urldecode($contra);
    $pass_decode = base64_decode($b64Contra);
    $pass_for_db = sha1($pass_decode);

    try {
        $db = new db();
        $db = $db->conectDB();
        $resultado = $db->prepare($sql);

        $resultado->bindParam(":correo", $correo);
        $resultado->bindParam(":contra", $pass_for_db);
        $resultado->execute();
        
        if ($resultado->rowCount() > 0) {
            $respUsuario = $resultado->fetchAll(PDO::FETCH_ASSOC);
            $session = new \SlimSession\Helper;
            $session['all'] = $respUsuario;
            $arrRes = array("ok" => true, "data" => $respUsuario);
            echo json_encode($arrRes);
        } else {
            $mensaje = "El usuario o la contraseña estan mal escritos o no existen.";
            $arrRes = array("ok" => false, "data" => $mensaje);
            echo json_encode($arrRes);
        }

        $resultado = null;
        $db = null;
    } catch(Exception $e) {
        $arr = array("ok" => false, "data" => $e->getMessage());
        echo json_encode($arr);
    }
});

//Cerrar sesión
$app->post('/api/logout', function (Request $request, Response  $response) use ($app) {
    $validation = checkAvailability($request);
    
    if ($validation !== false) {
        return $response->withRedirect($validation, 302);
    }

    $session = new \SlimSession\Helper;
    $session->delete('all');
    $session::destroy();
    $arrRes = array("ok" => true, "data" => "Se ha cerrado la sesión correctamente");
    echo json_encode($arrRes);
});

//Checar sesión iniciada
$app->post('/api/checkForSession', function (Request $request, Response  $response) use ($app) {
    $validation = checkAvailability($request);
    
    if ($validation !== false) {
        return $response->withRedirect($validation, 302);
    }

    $session = new \SlimSession\Helper;
    $exists = $session->exists('all');
    if (!$exists) {
        $mensaje = "No hay una sesión iniciada.";
        $arrRes = array("ok" => false, "data" => $mensaje);
        echo json_encode($arrRes);
    } else {
        $mensaje = "Sesión iniciada.";
        $arrRes = array("ok" => true, "data" => $mensaje);
        echo json_encode($arrRes);
    }
});

//Obtener todos los cursos
$app->get('/api/cursos', function(Request $request, Response  $response) use($app){
    $validation = checkAvailability($request);
    
    if ($validation !== false) {
        return $response->withRedirect($validation, 302);
    }

    $sql = "SELECT curso.id_curso, curso.nombre, curso.fecha, asistente.nombre AS \"instructor\", asistente.id_asistente,curso.cupo FROM curso LEFT JOIN asistente ON asistente.id_asistente = curso.id_asistente ORDER BY curso.id_curso";

    try{
        $db = new db();
        $db = $db->conectDB();
        $resultado = $db->prepare($sql);
        $resultado->execute();

        if($resultado->rowCount() > 0){
            $cursos = $resultado->fetchAll(PDO::FETCH_ASSOC);
            $arrRes = array("ok" => true, "data" => $cursos);
            echo json_encode($arrRes);
        }else{
            $mensaje = "No hay cursos en la BD";
            $arrRes = array("ok" => false, "data" => $mensaje);
            echo json_encode($arrRes);
        }

        $resultado = null;
        $db = null;
    }catch(Exception $e){
        $arr = array("ok" => false, "data" => $e->getMessage());
        echo json_encode($arr);
    }
});

//Obtener cursos inscritos
$app->get('/api/cursosins', function(Request $request, Response  $response) use($app){
    $parametros = $request->getQueryParams();
    $idAsistente = $parametros['id'];
    
    $validation = checkAvailability($request);
    
    if ($validation !== false) {
        return $response->withRedirect($validation, 302);
    }

    $sql = "SELECT asistente_curso.id_curso, asistente_curso.id_asistente, curso.fecha FROM asistente_curso JOIN curso ON curso.id_curso = asistente_curso.id_curso WHERE asistente_curso.id_asistente = :id_asistente";
    
    try{
        $db = new db();
        $db = $db->conectDB();
        $resultado = $db->prepare($sql);
        $resultado->bindParam(":id_asistente", $idAsistente);
        $resultado->execute();

        if($resultado->rowCount() > 0){
            $cursosIns = $resultado->fetchAll(PDO::FETCH_ASSOC);
            $arrRes = array("ok" => true, "data" => $cursosIns);
            echo json_encode($arrRes);
        }else{
            $mensaje = "No haz inscrito ningun curso";
            $arrRes = array("ok" => false, "data" => $mensaje);
            echo json_encode($arrRes);
        }

        $resultado = null;
        $db = null;
    }catch(Exception $e){
        $arr = array("ok" => false, "data" => $e->getMessage());
        echo json_encode($arr);
    }
});

//Inscribir curso
$app->post('/api/inscurso', function(Request $request, Response  $response) use($app){
    $parametros = $request->getQueryParams();
    $idCurso = $parametros['idc'];
    $idAsistente = $parametros['ida'];

    $validation = checkAvailability($request);
    
    if ($validation !== false) {
        return $response->withRedirect($validation, 302);
    }
    
    $sql = "INSERT INTO asistente_curso (id_asistente, id_curso) VALUES (:id_asistente, :id_curso)";
    $sqlCupo = "SELECT cupo FROM curso WHERE id_curso = :idc";
    $sqlDecrement = "UPDATE curso SET cupo = :cupo WHERE id_curso = :idc";
    $sqlConf = "SELECT * FROM asistente_curso WHERE id_asistente = :ida AND id_curso = :idc";
    $sqlCount = "SELECT COUNT(*) AS 'cantidad' FROM asistente_curso WHERE id_asistente = :ida";
    $sqlFechaCursosIns = "SELECT curso.fecha, asistente_curso.id_asistente FROM asistente_curso JOIN curso ON curso.id_curso = asistente_curso.id_curso WHERE asistente_curso.id_asistente = :ida";
    $sqlFechaCursoActual = "SELECT fecha, id_asistente FROM curso WHERE id_curso = :idc";
    $sqlInstAct = "SELECT id_asistente, fecha FROM curso WHERE id_asistente = :ida";


    try{
        $dbConf = new db();
        $dbConf = $dbConf->conectDB();
        $resConf = $dbConf->prepare($sqlConf);
        $resConf->bindParam(":ida", $idAsistente);
        $resConf->bindParam(":idc", $idCurso);
        $resConf->execute();

        if($resConf->rowCount() > 0){
            $mensaje = "Ya te haz inscrito a este curso.";
            $arrRes = array("ok" => false, "data" => $mensaje);
            echo json_encode($arrRes);
            return;
        }

        $dbConf = null;
        $resConf = null;

        $dbCount = new db();
        $dbCount = $dbCount->conectDB();
        $resCount = $dbCount->prepare($sqlCount);
        $resCount->bindParam(":ida", $idAsistente);
        $resCount->execute();

        if($resCount->rowCount() > 0){
            $count = $resCount->fetchAll(PDO::FETCH_ASSOC);
            $count = $count[0]['cantidad'];

            if ($count >= 5) {
                $mensaje = "Ya haz inscrito el máximo de cursos permitido.";
                $arrRes = array("ok" => false, "data" => $mensaje);
                echo json_encode($arrRes);
                return;
            }
        }

        $dbCount = null;
        $resCount = null;

        $dbFechaIns = new db();
        $dbFechaIns = $dbFechaIns->conectDB();
        $resFechaIns = $dbFechaIns->prepare($sqlFechaCursosIns);
        $resFechaIns->bindParam(":ida", $idAsistente);
        $resFechaIns->execute();

        if($resFechaIns->rowCount() > 0){
            $fecha = $resFechaIns->fetchAll(PDO::FETCH_ASSOC);
            $fecha = $fecha[0]['fecha'];
        }

        $dbFechaIns = null;
        $resFechaIns = null;

        $dbFechaAct = new db();
        $dbFechaAct = $dbFechaAct->conectDB();
        $resFechaAct = $dbFechaAct->prepare($sqlFechaCursoActual);
        $resFechaAct->bindParam(":idc", $idCurso);
        $resFechaAct->execute();

        if($resFechaAct->rowCount() > 0){
            $fechaActual = $resFechaAct->fetchAll(PDO::FETCH_ASSOC);
            $fechaAct = $fechaActual[0]['fecha'];
            $idInstructorDelCursoActual = $fechaActual[0]['id_asistente'];
        }

        if ($fecha == $fechaAct) {
            $mensaje = "No puedes inscribir dos cursos en el mismo dia.";
            $arrRes = array("ok" => false, "data" => $mensaje);
            echo json_encode($arrRes);
            return;
        }

        $dbFechaAct = null;
        $resFechaAct = null;

        $dbInstAct = new db();
        $dbInstAct = $dbInstAct->conectDB();
        $resInstAct = $dbInstAct->prepare($sqlInstAct);
        $resInstAct->bindParam(":ida", $idAsistente);
        $resInstAct->execute();

        if($resInstAct->rowCount() > 0){
            $resInstAct = $resInstAct->fetchAll(PDO::FETCH_ASSOC);
            if ($resInstAct[0]["id_asistente"] == $idInstructorDelCursoActual) {
                $mensaje = "Tú eres el instructor de este curso.";
                $arrRes = array("ok" => false, "data" => $mensaje);
                echo json_encode($arrRes);
                return;
            }
            if ($resInstAct[0]["fecha"] == $fechaAct) {
                $mensaje = "Los instructores no pueden inscribirse a cursos en el mismo día en el que impartirán el suyo.";
                $arrRes = array("ok" => false, "data" => $mensaje);
                echo json_encode($arrRes);
                return;
            }
        }
        
        $dbInstAct = null;
        $resInstAct = null;

        $dbCupo = new db();
        $dbCupo = $dbCupo->conectDB();
        $resCupo = $dbCupo->prepare($sqlCupo);
        $resCupo->bindParam(":idc", $idCurso);
        $resCupo->execute();

        if($resCupo->rowCount() > 0){
            $cupo = $resCupo->fetchAll(PDO::FETCH_ASSOC);
            $cupo = $cupo[0]['cupo'];
        }

        if ($cupo == 0) {
            $mensaje = "Este curso ya no tiene cupo.";
            $arrRes = array("ok" => false, "data" => $mensaje);
            echo json_encode($arrRes);
            return;
        } else {
            $cupo = $cupo - 1;
        }

        $dbCupo = null;
        $resCupo = null;
        $dbCupo = null;
        $resCupo = null;

        $dbCupoDec = new db();
        $dbCupoDec = $dbCupoDec->conectDB();
        $resCupoDec = $dbCupoDec->prepare($sqlDecrement);
        $resCupoDec->bindParam(":cupo", $cupo);
        $resCupoDec->bindParam(":idc", $idCurso);
        $resCupoDec->execute();

        $dbCupoDec = null;
        $resCupoDec = null;

        $db = new db();
        $db = $db->conectDB();
        $resultado = $db->prepare($sql);
        $resultado->bindParam(":id_asistente", $idAsistente);
        $resultado->bindParam(":id_curso", $idCurso);
        $resultado->execute();

        $res = array("ok" => true, "data" => "El usuario se ha inscrito con exito");
        echo json_encode($res);

        $resultado = null;
        $db = null;
    }catch(Exception $e){
        $arr = array("ok" => false, "data" => $e->getMessage());
        echo json_encode($arr);
    }
});

//Desinscribir curso
$app->post('/api/descurso', function(Request $request, Response  $response) use($app){
    $parametros = $request->getQueryParams();
    $idCurso = $parametros['idc'];
    $idAsistente = $parametros['ida'];

    $validation = checkAvailability($request);
    
    if ($validation !== false) {
        return $response->withRedirect($validation, 302);
    }
    
    $sql = "DELETE FROM asistente_curso WHERE id_asistente = :id_asistente AND id_curso = :id_curso";
    $sqlCupo = "SELECT cupo FROM curso WHERE id_curso = :idc";
    $sqlIncrement = "UPDATE curso SET cupo = :cupo WHERE id_curso = :idc";

    try{
        $dbCupo = new db();
        $dbCupo = $dbCupo->conectDB();
        $resCupo = $dbCupo->prepare($sqlCupo);
        $resCupo->bindParam(":idc", $idCurso);
        $resCupo->execute();

        if($resCupo->rowCount() > 0){
            $cupo = $resCupo->fetchAll(PDO::FETCH_ASSOC);
            $cupo = $cupo[0]['cupo'];
        }
        
        $cupo = $cupo + 1;

        $dbCupo = null;
        $resCupo = null;

        $dbCupoInc = new db();
        $dbCupoInc = $dbCupoInc->conectDB();
        $resCupoInc = $dbCupoInc->prepare($sqlIncrement);
        $resCupoInc->bindParam(":cupo", $cupo);
        $resCupoInc->bindParam(":idc", $idCurso);
        $resCupoInc->execute();

        $dbCupoInc = null;
        $resCupoInc = null;

        $db = new db();
        $db = $db->conectDB();
        $resultado = $db->prepare($sql);
        $resultado->bindParam(":id_asistente", $idAsistente);
        $resultado->bindParam(":id_curso", $idCurso);
        $resultado->execute();

        $res = array("ok" => true, "data" => "Curso dado de baja.");
        echo json_encode($res);

        $resultado = null;
        $db = null;
    }catch(Exception $e){
        $arr = array("ok" => false, "data" => $e->getMessage());
        echo json_encode($arr);
    }
});

//Obtener todos los usuarios
$app->get('/api/usrs', function(Request $request, Response  $response) use($app){

    $validation = checkAvailability($request);
    
    if ($validation !== false) {
        return $response->withRedirect($validation, 302);
    }

    $sql = "SELECT asistente.id_asistente, asistente.nombre, asistente.correo, curso.id_curso FROM asistente LEFT JOIN curso ON curso.id_asistente = asistente.id_asistente ORDER BY nombre";

    try{
        $db = new db();
        $db = $db->conectDB();
        $resultado = $db->prepare($sql);
        $resultado->execute();

        if($resultado->rowCount() > 0){
            $usuarios = $resultado->fetchAll(PDO::FETCH_ASSOC);
            $arrRes = array("ok" => true, "data" => $usuarios);
            echo json_encode($arrRes);
        }else{
            $mensaje = "No existen usuarios en la BD";
            $arrRes = array("ok" => false, "data" => $mensaje);
            echo json_encode($arrRes);
        }

        $resultado = null;
        $db = null;
    }catch(Exception $e){
        $arr = array("ok" => false, "data" => $e->getMessage());
        echo json_encode($arr);
    }
});

//Asignar curso
$app->post('/api/ascurso', function(Request $request, Response  $response) use($app){
    $parametros = $request->getQueryParams();
    $idCurso = $parametros['idc'];
    $idAsistente = $parametros['ida'];

    $validation = checkAvailability($request);
    
    if ($validation !== false) {
        return $response->withRedirect($validation, 302);
    }

    $sql = "UPDATE curso SET id_asistente = :ida WHERE id_curso = :idc";
    $sqlConf = "SELECT id_curso FROM curso WHERE id_asistente = :ida";
    $sqlRang = "UPDATE asistente SET id_rango = 1 WHERE id_asistente = :ida";

    try{
        $dbConf = new db();
        $dbConf = $dbConf->conectDB();
        $resConf = $dbConf->prepare($sqlConf);
        $resConf->bindParam(':ida', $idAsistente);
        $resConf->execute();

        if($resConf->rowCount() > 0){
            $arrRes = array("ok" => false, "data" => "Instructor con curso ya asignado");
            echo json_encode($arrRes);
            return;
        }

        $dbConf = null;
        $resConf = null;

        $db = new db();
        $db = $db->conectDB();
        $resultado = $db->prepare($sql);
        $resultado->bindParam(':ida', $idAsistente);
        $resultado->bindParam(':idc', $idCurso);
        $resultado->execute();

        $dbRang = new db();
        $dbRang = $dbRang->conectDB();
        $resRang = $dbRang->prepare($sqlRang);
        $resRang->bindParam(':ida', $idAsistente);
        $resRang->execute();

        $arrRes = array("ok" => true, "data" => "Curso asignado con exito");
        echo json_encode($arrRes);

        $resultado = null;
        $db = null;
    }catch(Exception $e){
        $arr = array("ok" => false, "data" => $e->getMessage());
        echo json_encode($arr);
    }
});

//Eliminar profesor del curso
$app->post('/api/elinscu', function(Request $request, Response  $response) use($app){
    $parametros = $request->getQueryParams();
    $idCurso = $parametros['idc'];
    $idAsistente = $parametros['ida'];

    $validation = checkAvailability($request);
    
    if ($validation !== false) {
        return $response->withRedirect($validation, 302);
    }

    $sql = "UPDATE curso SET id_asistente = NULL WHERE id_curso = :idc";
    $sqlRang = "UPDATE asistente SET id_rango = 2 WHERE id_asistente = :ida";

    try{
        $db = new db();
        $db = $db->conectDB();
        $resultado = $db->prepare($sql);
        $resultado->bindParam(':idc', $idCurso);
        $resultado->execute();

        $dbRang = new db();
        $dbRang = $dbRang->conectDB();
        $resRang = $dbRang->prepare($sqlRang);
        $resRang->bindParam(':ida', $idAsistente);
        $resRang->execute();

        $dbRang = null;
        $resRang = null;

        $arrRes = array("ok" => true, "data" => "Instructor desasignado");
        echo json_encode($arrRes);

        $resultado = null;
        $db = null;
    }catch(Exception $e){
        $arr = array("ok" => false, "data" => $e->getMessage());
        echo json_encode($arr);
    }
});

//Obtener rango
$app->get('/api/rango', function(Request $request, Response  $response) use($app){
    $parametros = $request->getQueryParams();
    $idAsistente = $parametros['ida'];

    $validation = checkAvailability($request);
    
    if ($validation !== false) {
        return $response->withRedirect($validation, 302);
    }

    $rango = getRango($idAsistente);

    if ($rango == 0) {
        $mensaje = "El id del parámetro no es el correcto.";
        $arrRes = array("ok" => false, "data" => $mensaje);
        echo json_encode($arrRes);
        $resultado = null;
        $db = null;
        return;
    } else {
        $arrRes = array("ok" => true, "data" => $rango);
        echo json_encode($arrRes);
    }
    
});

//Método auxiliar para obtener rango
function getRango($idAsistente) {
    $sql = "SELECT id_rango FROM asistente WHERE id_asistente = :ida";

    try{
        $db = new db();
        $db = $db->conectDB();
        $resultado = $db->prepare($sql);
        $resultado->bindParam(':ida', $idAsistente);
        $resultado->execute();

        if($resultado->rowCount() > 0){
            $session = new \SlimSession\Helper;
            $exists = $session->exists('all');
            if ($exists) {
                $rangoCookie = intval($session['all'][0]['id_rango']);
            }
            $rango = $resultado->fetchAll(PDO::FETCH_ASSOC);
            $session['all'][0]['id_rango'] = "9";
            
            if ($rango != 3) {
                if (intval($rango[0]['id_rango']) != $rangoCookie) {
                    $db = null;
                    $resultado = null;
                    return 0;
                } 
                return $rango;
            }
        }else{
            $mensaje = "Por alguna razon este usuario no tiene rango";
            $arrRes = array("ok" => false, "data" => $mensaje);
            echo json_encode($arrRes);
        }

        $resultado = null;
        $db = null;
    }catch(Exception $e){
        $arr = array("ok" => false, "data" => $e->getMessage());
        echo json_encode($arr);
    }
}

//Obtener curso por profesor
$app->get('/api/getcrs', function(Request $request, Response  $response) use($app){
    $parametros = $request->getQueryParams();
    $idAsistente = $parametros['ida'];

    $validation = checkAvailability($request);
    
    if ($validation !== false) {
        return $response->withRedirect($validation, 302);
    }

    $sqlCursos = "SELECT curso.id_curso, curso.nombre FROM curso WHERE curso.id_asistente = :ida";
    $sqlAsistentes = "SELECT asistente.id_asistente, asistente.nombre, asistente_curso.asistencia FROM asistente JOIN asistente_curso ON asistente.id_asistente = asistente_curso.id_asistente WHERE asistente_curso.id_curso = :idc";
    $sqlPonente = "SELECT asistente.nombre FROM asistente WHERE asistente.id_asistente = :ida";

    $rango = getRango($idAsistente);

    if ($rango == 0) {
        $mensaje = "El id del parámetro no es el correcto.";
        $arrRes = array("ok" => false, "data" => $mensaje);
        echo json_encode($arrRes);
        return;
    }

    $rangoInt = intval($rango[0]['id_rango']);

    try{
        $db = new db();
        $db = $db->conectDB();

        $resultado = $db->prepare($sqlCursos);
        $resultado->bindParam(':ida', $idAsistente);
        $resultado->execute();

        
        if ($resultado->rowCount() > 0) {
            $curso = $resultado->fetchAll(PDO::FETCH_ASSOC);
            $nombreCurso = $curso[0]['nombre'];
            $idCurso = $curso[0]['id_curso'];
        } else {
            $mensaje = "No hay cursos para este usuario";
            $arrRes = array("ok" => false, "data" => $mensaje);
            echo json_encode($arrRes);
            $db = null;
            $resultado = null;
            return;
        } 

        $resPonente = $db->prepare($sqlPonente);
        $resPonente->bindParam(':ida', $idAsistente);
        $resPonente->execute();
        
        if ($resPonente->rowCount() > 0) {
            $ponente = $resPonente->fetchAll(PDO::FETCH_ASSOC);
            $nombrePonente = $ponente[0]['nombre'];
        } else {
            $mensaje = "Ocurrió un error, no se encuentra el nombre del id enviado.";
            $arrRes = array("ok" => false, "data" => $mensaje);
            echo json_encode($arrRes);
            return;
        } 
        
        $resAsistentes = $db->prepare($sqlAsistentes);
        $resAsistentes->bindParam(':idc', $idCurso);
        $resAsistentes->execute();

        if ($resAsistentes->rowCount() > 0) {
            $asistentes = $resAsistentes->fetchAll(PDO::FETCH_ASSOC);
            $arrRes = array("ok" => true, "data" => $asistentes, "curso" => $idCurso, "nombre" => $nombreCurso, "instructor" => $nombrePonente);
            echo json_encode($arrRes);
        } else {
            $mensaje = "No hay usuarios inscritos en este curso aún.";
            $arrRes = array("ok" => false, "data" => $mensaje);
            echo json_encode($arrRes);
        } 

        $db = null;
        $resultado = null;
        $resAsistentes = null;
    }catch(Exception $e){
        $arr = array("ok" => false, "data" => $e->getMessage());
        echo json_encode($arr);
    }
});

//Registro de asistencias
$app->post('/api/regasist', function(Request $request, Response  $response) use($app){
    $parametros = $request->getParsedBody();

    $validation = checkAvailability($request);
    
    if ($validation !== false) {
        return $response->withRedirect($validation, 302);
    }

    $alumnos = $parametros['body'];
    $idCurso = $parametros['curso'];

    $sqlAsistencia = "UPDATE asistente_curso SET asistente_curso.asistencia = :asistencia WHERE asistente_curso.id_asistente = :ida AND asistente_curso.id_curso = :idc";

    try{
        $db = new db();
        $db = $db->conectDB();

        for ($i=0; $i < sizeof($alumnos); $i++) { 
            if ($alumnos[$i]['asistencia'] == true){
                $asistencia = 1;
            } else {
                $asistencia = 0;
            }
        
            $resultado = $db->prepare($sqlAsistencia);
            $resultado->bindParam(':asistencia', $asistencia);
            $resultado->bindParam(':ida', $alumnos[$i]['id']);
            $resultado->bindParam(':idc', $idCurso);
            $resultado->execute();
        }

        $arrRes = array("ok" => true, "data" => "Asistencias actualizadas con éxito.");
        echo json_encode($arrRes);

        $db = null;
        $resultado = null;
    }catch(Exception $e){
        $arr = array("ok" => false, "data" => $e->getMessage());
        echo json_encode($arr);
    }
});

//Servicio para reportes
$app->get('/api/repor', function(Request $request, Response  $response) use($app){
    $parametros = $request->getQueryParams();
    $tipo = $parametros['t'];
    $idCurso = $parametros['idc'];
    $asistencia = $parametros['a'];
    $origin = $parametros['or'];
    $validation = checkAvailability($request);
    
    if ($validation !== false) {
        return $response->withRedirect($validation, 302);
    }
    
    $sql = "";
    
    if (!IsNullOrEmptyString($idCurso)) {
        $courBased = true;
    }

    if ($tipo == 'gen') {
        $sql = "SELECT asistente.id_asistente, asistente.nombre, asistente.inst_procedencia, asistente.perfil_academico, estado.nombre AS estado, asistente.correo, COUNT(curso.nombre) AS cursos_inscritos FROM asistente JOIN estado ON estado.id_estado = asistente.id_estado LEFT JOIN asistente_curso ON asistente_curso.id_asistente = asistente.id_asistente LEFT JOIN curso ON curso.id_curso = asistente_curso.id_curso GROUP BY asistente.id_asistente";
        $filename = "reporte_general";
    }

    if ($courBased) {
        $sql = "SELECT asistente.id_asistente, asistente.nombre, asistente.inst_procedencia, asistente.perfil_academico, estado.nombre AS estado, asistente.correo, asistente_curso.asistencia FROM asistente JOIN estado ON estado.id_estado = asistente.id_estado LEFT JOIN asistente_curso ON asistente_curso.id_asistente = asistente.id_asistente LEFT JOIN curso ON curso.id_curso = asistente_curso.id_curso WHERE curso.id_curso = :idc";
        $sqlNomCurso = "SELECT curso.nombre FROM curso WHERE curso.id_curso = :idc";
        
        if (!IsNullOrEmptyString($asistencia)) {
            if ($asistencia == "as") {
                $sql = $sql . " AND asistente_curso.asistencia = 1";
            } 
            if ($asistencia == "ns") {
                $sql = $sql . " AND asistente_curso.asistencia = 0";
            } 
        }
    }

    if ($tipo == "inst") {
        $sql = "SELECT curso.id_asistente, asistente.nombre, curso.nombre AS curso_impartido, asistente.correo, curso.cupo FROM curso LEFT JOIN asistente ON asistente.id_asistente = curso.id_asistente";
        $filename = "reporte_instructores";
    }
    $nombre = "";
    try{
        $db = new db();
        $db = $db->conectDB();
        $resultado = $db->prepare($sql);
        if ($courBased) {
            $resNomCurso = $db->prepare($sqlNomCurso);
            $resNomCurso->bindParam('idc', $idCurso);
            $resNomCurso->execute();

            if($resNomCurso->rowCount() > 0){
                $nombreCurso = $resNomCurso->fetchAll(PDO::FETCH_ASSOC);
                $filename = $nombreCurso[0]['nombre'];
            }
            $resNomCurso = null;

            $resultado->bindParam('idc', $idCurso);
        }
        $resultado->execute();
        
        if($resultado->rowCount() > 0){
            $info = $resultado->fetchAll(PDO::FETCH_ASSOC);
            if ($origin == "xls") {
                $filename = removeAccents($filename);
                $filename = str_replace(' ', '', $filename);
                exportToExcel($info,$filename);
                return;
            }
            $arrRes = array("ok" => true, "data" => $info);
            echo json_encode($arrRes);
        }else{
            $mensaje = "No hay informacion aun en la base de datos";
            $arrRes = array("ok" => false, "data" => $mensaje);
            echo json_encode($arrRes);
        }

        $resultado = null;
        $db = null;
    }catch(Exception $e){
        $arr = array("ok" => false, "data" => $e->getMessage());
        echo json_encode($arr);
    }
});

function removeAccents($str) {
    $unwanted_array = array('Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
                            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U',
                            'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c',
                            'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
                            'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y');
    return strtr($str, $unwanted_array);
}

//Función auxiliar para exportar a excel
function exportToExcel($data, $filename) {
    $spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();
    $sheet->fromArray($data, NULL, 'A1');

    header('Content-Type: application/vnd.ms-excel');
    header("Content-Disposition: attachment; filename={$filename}.xls");
    header('Cache-Control: max-age=0');
    $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xls');
    $writer->save('php://output');
}

//Obtener conferencias
$app->get('/api/conferencias', function(Request $request, Response  $response) use($app){
    $validation = checkAvailability($request);
    
    if ($validation !== false) {
        return $response->withRedirect($validation, 302);
    }

    $sql = "SELECT * FROM curso";

    try{
        $db = new db();
        $db = $db->conectDB();
        $resultado = $db->prepare($sql);
        $resultado->execute();

        $conferenciaActual = array();

        if($resultado->rowCount() > 0){
            $conferencias = $resultado->fetchAll(PDO::FETCH_ASSOC);
            date_default_timezone_set('America/Mexico_City');

            usort($conferencias, function($a, $b) {
                return strtotime($a['fecha']) <=> strtotime($b['fecha']);
            });

            $fechaActual = time();
            // $fechaPruebas = strtotime('2021-11-23 18:01:00');
            // $fechaActual = $fechaPruebas;

            for ($i=0; $i < sizeof($conferencias); $i++) { 
                $fechaConferencia = strtotime($conferencias[$i]['fecha']);
                $fechaTerminoConferencia = strtotime($conferencias[$i]['fecha_termino']);
                
                if ($fechaActual > $fechaConferencia && $fechaActual < $fechaTerminoConferencia) {
                    $temp = array('nombre_conferencia' => $conferencias[$i]['nombre'], 'fecha' => $conferencias[$i]['fecha'], 'sala' => $conferencias[$i]['sala']); 
                    array_push($conferenciaActual, $temp);
                }
            }
            
            if ($conferenciaActual != null) {
                $arrRes = array("ok" => true, "data" => $conferenciaActual);
            } else {
                $arrRes = array("ok" => false, "data" => "No hay conferencias en este momento"); 
            }
            
            echo json_encode($arrRes);
        }else{
            $mensaje = "No hay conferencias en la BD";
            $arrRes = array("ok" => false, "data" => $mensaje);
            echo json_encode($arrRes);
        }

        $resultado = null;
        $db = null;
    }catch(Exception $e){
        $arr = array("ok" => false, "data" => $e->getMessage());
        echo json_encode($arr);
    }
});

$app->get('/api/genpdf', function(Request $request, Response  $response) use($app){
    $validation = checkAvailability($request);
    $parametros = $request->getQueryParams();
    $idAsistente = $parametros['ida'];
    
    $img = file_get_contents("http://localhost:8888/reunionanualback/public/x3bnmaxub0.jpg");
    $data = base64_encode($img);
    $extension = pathinfo(parse_url("http://localhost:8888/reunionanualback/public/x3bnmaxub0.jpg", PHP_URL_PATH), PATHINFO_EXTENSION);
    $base64 = 'data:image/' . $extension . ';base64,' . $data;
    $html = "<div style='width:80%;height:80%;'><img src=". $base64 ." style='width: 630pt;margin-left:80pt'><h3>Ooo</h3></div>";
    

    $html2pdf = new \Spipu\Html2Pdf\Html2Pdf('P', 'A4', 'en');
    $html2pdf->writeHTML($html);

    $html2pdf->output();
    // try{     
    //     echo json_encode($arrRes);
    //     $resultado = null;
    //     $db = null;
    // }catch(Exception $e){
    //     $arr = array("ok" => false, "data" => $e->getMessage());
    //     echo json_encode($arr);
    // }
});

// Catch-all route to serve a 404 Not Found page if none of the routes match
// NOTE: make sure this route is defined last
$app->map(['GET', 'POST', 'PUT', 'DELETE', 'PATCH'], '/{routes:.+}', function ($req, $res) {
    $handler = $this->notFoundHandler; // handle using the default Slim page not found handler
    return $handler($req, $res);
});